defmodule ConverterApiWeb.Acceptance.TemperatureTest do
  use ExUnit.Case, async: true
  use Wallaby.Feature

  describe "convert celsius to farenheit" do
    @tag acceptance: true
    feature "valid params", %{session: session} do
      session
      |> visit("/?conversor=c2f&value=36.5")
      |> find(Query.text("36.5 convertido de c2f = 97.7"))
    end

    @tag acceptance: true
    feature "missing conversor params", %{session: session} do
      session
      |> visit("/?value=36.5")
      |> find(Query.text("Conversor inválido"))
    end

    @tag acceptance: true
    feature "missing value params", %{session: session} do
      session
      |> visit("/?conversor=c2f")
      |> find(Query.text("Valor inválido"))
    end
  end

  describe "convert celsius to kelvin" do
    @tag acceptance: true
    feature "valid params", %{session: session} do
      session
      |> visit("/?conversor=c2k&value=0")
      |> find(Query.text("0 convertido de c2k = 273.15"))
    end
  end
end
