defmodule ConverterApi.CelsiusTest do
  # Example using Outline format
  # use Cabbage.Feature, async: false, file: "celsius_outline.feature"
  use Cabbage.Feature, async: false, file: "celsius.feature"

  defgiven ~r/^que o conversor está ativo/,
           _,
           state do
    {:ok, state}
  end

  defwhen ~r/^for informado "(?<valor>[^"]+)"/, %{valor: value}, state do
    {:ok, Map.merge(state, %{value: value})}
  end

  defwhen ~r/^a medida indicada for "(?<medida>[^"]+)"/, %{medida: measure}, state do
    {:ok, Map.merge(state, %{measure: measure})}
  end

  defthen ~r/^o valor convertido será "(?<valor_convertido>[^"]+)"/,
          %{valor_convertido: converted_value},
          state do
    {:ok, {_value, converted}} = ConverterApi.Celsius.convert(state.value, state.measure)

    assert converted_value == "#{converted}"
  end
end
