defmodule ConverterApiWeb.Router do
  use ConverterApiWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
    plug(CORSPlug)
  end

  pipeline :browser do
    plug(:accepts, ["html"])
    plug :fetch_session
    plug :put_root_layout, html: {ConverterApiWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", ConverterApiWeb do
    pipe_through :browser

    get("/", TemperatureController, :show)
  end

  scope "/api", ConverterApiWeb do
    pipe_through(:api)

    get("/temperature", TemperatureController, :conversor)
  end
end
