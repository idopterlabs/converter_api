defmodule ConverterApiWeb.TemperatureController do
  use ConverterApiWeb, :controller

  alias ConverterApi.Celsius

  def show(conn, params) do
    conversor = Map.get(params, "conversor")
    value = Map.get(params, "value")

    case Celsius.convert(value, conversor) do
      {:ok, {value, converted_value}} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(201, "#{value} convertido de #{conversor} = #{converted_value}")

      {:error, :invalid_number} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(201, "Valor inválido")

      {:error, :invalid_conversor} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(201, "Conversor inválido")

      _ ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(201, "Parâmetros inválidos")
    end
  end

  def conversor(conn, %{"conversor" => "f2k", "value" => value} = _params) do
    case parse_value(value) do
      {:ok, value} ->
        # k = (f - 32) * 5/9 + 273,15
        converted_value_celsius = (value - 32) * 5 / 9
        converted_value = converted_value_celsius + 273.15

        json(conn, %{original_value: value, converted_value: converted_value, unit: "kelvin"})

      _ ->
        handle_error(conn)
    end
  end

  def conversor(conn, %{"conversor" => "c2k", "value" => value} = _params) do
    case parse_value(value) do
      {:ok, value} ->
        converted_value = value + 273.15

        json(conn, %{original_value: value, converted_value: converted_value, unit: "kelvin"})

      _ ->
        handle_error(conn)
    end
  end

  def conversor(conn, %{"conversor" => "f2c", "value" => value} = _params) do
    case parse_value(value) do
      {:ok, value} ->
        converted_value = (value - 32) * 5 / 9

        json(conn, %{original_value: value, converted_value: converted_value, unit: "celsius"})

      _ ->
        handle_error(conn)
    end
  end

  def conversor(conn, %{"conversor" => "c2f" = conversor, "value" => value} = _params) do
    case Celsius.convert(value, conversor) do
      {:ok, {value, converted_value}} ->
        json(conn, %{original_value: value, converted_value: converted_value, unit: "fahrenheit"})

      _ ->
        handle_error(conn)
    end
  end

  def conversor(conn, _params) do
    handle_error(conn)
  end

  defp handle_error(conn) do
    json(conn, %{
      error: ~S"""
        Invalid conversor! The options available are:

        - f2c (Farenheit to Celsius)
        - c2f (Celsius to Farenheit)
      """
    })
  end

  defp parse_value(value) do
    integer_parse = Integer.parse(value)

    case integer_parse do
      {int, ""} -> {:ok, String.to_float("#{int}.0")}
      {_int, _decimal} -> {:ok, String.to_float(value)}
      :error -> {:error, value}
    end
  end
end
