defmodule ConverterApi.Parser do
  @moduledoc """
    This module has parsers for different values
  """

  def number!(value) do
    case number(value) do
      {:ok, number} -> number
      _ -> nil
    end
  end

  def number(value) do
    integer_parse = Integer.parse(value)

    case integer_parse do
      {int, ""} -> {:ok, String.to_float("#{int}.0")}
      {_int, _decimal} -> {:ok, String.to_float(value)}
      :error -> {:error, value}
    end
  end
end
