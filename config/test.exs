import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :converter_api, ConverterApiWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "p9jOM8v8zL/Bckin+fMMf4EUh+OW+5EABcbfLLWYtzpB9Yu3LfnVIK1BSYkY/2V4",
  server: true

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

# config :phoenix_live_view,
# Enable helpful, but potentially expensive runtime checks
#   enable_expensive_runtime_checks: true
#

# if System.get_env("E2E_ENABLE") do
config :wallaby,
  driver: Wallaby.Chrome,
  chromedriver: [headless: false]

# end
