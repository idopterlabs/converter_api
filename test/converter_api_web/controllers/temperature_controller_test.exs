defmodule ConverterApiWeb.TemperatureControllerTest do
  use ConverterApiWeb.ConnCase

  describe "f2c conversor" do
    test "float value", %{conn: conn} do
      value = 36.5

      conn = get(conn, "/api/temperature?conversor=f2c&value=#{value}")

      assert json_response(conn, 200) == %{
               "converted_value" => 2.5,
               "original_value" => 36.5,
               "unit" => "celsius"
             }
    end

    test "int value", %{conn: conn} do
      value = 32

      conn = get(conn, "/api/temperature?conversor=f2c&value=#{value}")

      assert json_response(conn, 200) == %{
               "converted_value" => 0,
               "original_value" => 32,
               "unit" => "celsius"
             }
    end

    test "invalid value", %{conn: conn} do
      value = "blah"

      conn = get(conn, "/api/temperature?conversor=f2c&value=#{value}")

      assert %{"error" => error} = json_response(conn, 200)

      assert error =~ "Invalid conversor!"
    end
  end

  describe "c2f conversor" do
    test "float value", %{conn: conn} do
      value = 36.5

      conn = get(conn, "/api/temperature?conversor=c2f&value=#{value}")

      assert json_response(conn, 200) == %{
               "converted_value" => 97.7,
               "original_value" => 36.5,
               "unit" => "fahrenheit"
             }
    end

    test "int value", %{conn: conn} do
      value = 32

      conn = get(conn, "/api/temperature?conversor=c2f&value=#{value}")

      assert json_response(conn, 200) == %{
               "converted_value" => 89.6,
               "original_value" => 32,
               "unit" => "fahrenheit"
             }
    end

    test "invalid value", %{conn: conn} do
      value = "blah"

      conn = get(conn, "/api/temperature?conversor=c2f&value=#{value}")

      assert %{"error" => error} = json_response(conn, 200)

      assert error =~ "Invalid conversor!"
    end
  end

  describe "c2k conversor" do
    test "float value", %{conn: conn} do
      value = 36.5

      # k = c + 273.15
      conn = get(conn, "/api/temperature?conversor=c2k&value=#{value}")

      assert json_response(conn, 200) == %{
               "converted_value" => 309.65,
               "original_value" => 36.5,
               "unit" => "kelvin"
             }
    end
  end

  describe "f2k conversor" do
    test "int value", %{conn: conn} do
      value = 36

      # k = (f - 32) * 5/9 + 273,15
      conn = get(conn, "/api/temperature?conversor=f2k&value=#{value}")

      assert json_response(conn, 200) == %{
               "converted_value" => 275.3722222222222,
               "original_value" => 36,
               "unit" => "kelvin"
             }
    end
  end
end
