ExUnit.start()

{:ok, _} = Application.ensure_all_started(:wallaby)

# if System.get_env("E2E_ENABLE") do
Application.put_env(:wallaby, :base_url, ConverterApiWeb.Endpoint.url())
# end
