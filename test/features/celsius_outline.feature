Feature: Converter de Celsius para outras medidas
	Como usuário
	Eu gostaria de informar o valor em uma determinada medida
	E indicar a medida de conversão
	Para que fosse apresentado o valor convertido

  Scenario Outline: Celsius para Farenheit
		Given que o conversor está ativo
		When for informado "<valor>"
		And a medida indicada for "<medida>"
		Then o valor convertido será "<resultado>"

		Examples:
			| valor			| medida			| resultado   |
			| 36.5		  |	c2f		      | 	97.7   		|
			| 0		      | c2f	        |	  32.0	    |
