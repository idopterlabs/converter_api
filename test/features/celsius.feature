Feature: Converter de Celsius para outras medidas
	Como usuário
	Eu gostaria de informar o valor em uma determinada medida
	E indicar a medida de conversão
	Para que fosse apresentado o valor convertido

	Scenario: Celsius para Farenheit
		Given que o conversor está ativo
		When for informado "36.5"
		And a medida indicada for "c2f"
		Then o valor convertido será "97.7"

	Scenario: Celsius para Farenheit outro valor
		Given que o conversor está ativo
		When for informado "32"
		And a medida indicada for "c2f"
		Then o valor convertido será "89.6"

	Scenario: Celsius para Farenheit com valor zero
		Given que o conversor está ativo
		When for informado "0"
		And a medida indicada for "c2f"
		Then o valor convertido será "32.0"

	Scenario: Celsius para Kelvin com valor zero
		Given que o conversor está ativo
		When for informado "0"
		And a medida indicada for "c2k"
		Then o valor convertido será "273.15"

	Scenario: Celsius para Kelvin
		Given que o conversor está ativo
		When for informado "36"
		And a medida indicada for "c2k"
		Then o valor convertido será "309.15"

	Scenario: Celsius para Kelvin com valor negativo
		Given que o conversor está ativo
		When for informado "-6.8"
		And a medida indicada for "c2k"
		Then o valor convertido será "266.35"
