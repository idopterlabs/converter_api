defmodule ConverterApi.Celsius do
  @moduledoc """
    This module has celsius conversor to other measures
  """

  alias ConverterApi.Parser

  def convert(nil, _), do: {:error, :invalid_number}

  def convert(_, nil), do: {:error, :invalid_conversor}

  def convert(value, "c2f") do
    case Parser.number(value) do
      {:ok, value} ->
        converted_value = value * 9 / 5 + 32

        {:ok, {value, converted_value}}

      _ ->
        {:error, :invalid_number}
    end
  end

  def convert(value, "c2k") do
    celsius = Parser.number!(value)
    kelvin = Float.round(celsius + 273.15, 2)

    {:ok, {value, kelvin}}
  end
end
